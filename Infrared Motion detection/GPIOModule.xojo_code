#tag Module
Protected Module GPIOModule
	#tag Note, Name = Notes
		  from Eugene Darkin.
		
		His library can be found at https://github.com/eugenedakin/pigpio-GPIO
		
		Eugene also has excellent book on GPIO for Xojo called: 
		Program Raspberry Pi 4B Electronics with Xojo
		
		His book and several others can be found at:
		https://scispec.ca/index.php/books/54-program-raspberry-pi-4b-electronics-with-xojo-buster-edition
		
		
		Our goal by havine separate library was to simplify many things for our examples 
		and make the Syntax more Xojo like as well as to guaranty our Examples continue to work.
		
		
		This library is not complete, we are just adding things on case by case baseis
		as things are needed for our GPIO Examples.
		This library is partially based onpigpio-GPIO
	#tag EndNote


End Module
#tag EndModule
