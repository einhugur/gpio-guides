#tag Module
Protected Module InteruptModule
	#tag Method, Flags = &h0
		Sub SensorInterupt(handle as Integer, pin as UInteger, level as UInteger, tick as UInt32)
		  // We need some pragmas here to make this safe since this is called from interupt
		  #Pragma StackOverflowChecking False
		  #Pragma DisableBackgroundTasks
		  
		  // Suppress some warnings since we do not use all of the parameters
		  #Pragma Unused handle
		  #Pragma Unused pin
		  #Pragma Unused level
		  #Pragma Unused tick
		  
		  // We only count the events here since it is not safe to call functions in interupt
		  DetectionCount = DetectionCount + 1
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		DetectionCount As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="DetectionCount"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
