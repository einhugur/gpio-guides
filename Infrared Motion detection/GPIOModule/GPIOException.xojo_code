#tag Class
Class GPIOException
Inherits RuntimeException
	#tag Method, Flags = &h0
		Sub Constructor(mode as Operation, pin as UInteger, result as Integer)
		  self.Mode = mode
		  self.Result = result
		  self.Pin = pin
		End Sub
	#tag EndMethod


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  select case Result
			  case PI_GPIO.BAD_GPIO
			    return "Bad GPIO pin: " + Str(pin)
			    
			  case PI_GPIO.BAD_MODE
			    return "Bad mode on pin: " + Str(pin)
			    
			  case PI_GPIO.BAD_LEVEL
			    return "Bad level on pin: " + Str(pin)
			    
			  case PI_GPIO.BAD_PUD
			    return "Bad pull up or down value on pin: " + Str(pin)
			    
			  case PI_GPIO.BAD_MALLOC
			    if Mode = Operation.RegisterCallback then
			      return "Bad Malloc when registering callback on pin: " + Str(pin)
			    else
			      return "Bad Malloc on pin: " + Str(pin)
			    end if
			    
			  case PI_GPIO.DUPLICATE_CALLBACK
			    return "Duplicate callback registered on pin: " + Str(pin)
			    
			    
			  case PI_GPIO.BAD_CALLBACK
			    return "Bad callback registered on pin: " + Str(pin)
			    
			  case PI_GPIO.CALLBACK_NOTFOUND
			    return "Callback not found on pin: " + Str(pin)
			    
			  case else
			    return "Unknown error (" + Str(Result) + ")"
			  end select
			  
			  
			End Get
		#tag EndGetter
		Message As String
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Mode As Operation
	#tag EndProperty

	#tag Property, Flags = &h0
		Pin As UInteger
	#tag EndProperty

	#tag Property, Flags = &h0
		Result As Integer
	#tag EndProperty


	#tag Enum, Name = Operation, Type = Integer, Flags = &h0
		SetMode=0
		  Write=1
		  SetPullUpDown=2
		  RegisterCallback=3
		UnregisterCallback=4
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="ErrorNumber"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Message"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Reason"
			Group="Behavior"
			Type="Text"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Mode"
			Group="Behavior"
			Type="Operation"
			EditorType="Enum"
			#tag EnumValues
				"0 - SetMode"
				"1 - Write"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Result"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Pin"
			Group="Behavior"
			Type="UInteger"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
