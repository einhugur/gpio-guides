#tag Class
Class PI_GPIO
	#tag Method, Flags = &h21
		Private Sub Constructor()
		  #If TargetARM And TargetLinux Then
		    soft Declare Function gpioInitialise Lib "pigpio.so" () as Integer
		    Dim RetVal as Integer
		    Dim s as shell
		    s = New Shell
		    s.Execute("sudo killall pigpiod")
		    LibraryVersion = gpioInitialise()
		    If (LibraryVersion = PI_INIT_FAILED) Then
		      // pigpio initialisation failed.
		      //Kill all pigpiod and try to reinitialize
		      s = New Shell
		      s.Execute("sudo killall pigpiod")
		      LibraryVersion = gpioInitialise()
		    End If
		  #Endif
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function Create() As PI_GPIO
		  Dim result as PI_GPIO = new PI_GPIO()
		  
		  return if(result.LibraryVersion > 0, result, nil)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Destructor()
		  #If TargetARM And TargetLinux Then
		    soft Declare Sub gpioTerminate Lib "pigpio.so" ()
		    gpioTerminate()
		  #Endif
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function RegisterCallback(handle as Integer, pin as UInteger, edge as UInteger, f as Ptr) As Integer
		  //This function initialises a new callback.
		  //pi: >=0 (as returned by [*pigpio_start*]).
		  //user_gpio: 0-31.
		  //edge: RISING_EDGE, FALLING_EDGE, or EITHER_EDGE.
		  //f: the callback function.
		  //The function returns a callback id if OK, otherwise pigif_bad_malloc,
		  //pigif_duplicate_callback, or pigif_bad_callback.
		  //The callback is called with the GPIO, edge, and tick, whenever the
		  //GPIO has the identified edge.
		  //Parameter   Value    Meaning
		  //GPIO        0-31     The GPIO which has changed state
		  //edge        0-2      0 = change to low (a falling edge)
		  //1 = change to high (a rising edge)
		  //2 = no level change (a watchdog timeout)
		  //tick        32 bit   The number of microseconds since boot
		  //WARNING: this wraps around from
		  //4294967295 to 0 roughly every 72 minutes
		  #If TargetARM And TargetLinux Then
		    Soft Declare Function callback Lib "libpigpiod_if2.so" (pi as Integer, pin as UInteger, edge as UInteger, f as Ptr) as Integer
		    Dim result as Integer = callback(handle, pin, edge, f)
		    
		    if result < 0 then
		      if result <> 0 then
		        Raise new GPIOException(GPIOException.Operation.RegisterCallback, pin, result)
		      end if
		    end if
		    
		    return result
		  #Else
		    #Pragma Unused handle
		    #Pragma Unused pin
		    #Pragma Unused edge
		    #Pragma Unused f
		  #Endif
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetMode(handle as Integer, pin as UInteger, mode as UInteger)
		  //Sets the GPIO mode, typically input or output.
		  //pin: 0-53
		  //mode: 0-7
		  //Returns 0 if OK, otherwise PI_BAD_GPIO or PI_BAD_MODE.
		  //Arduino style: pinMode.
		  //gpioSetMode(17, PI_INPUT);  // Set GPIO17 as input.
		  //gpioSetMode(18, PI_OUTPUT); // Set GPIO18 as output.
		  //gpioSetMode(22, PI_ALT0);    // Set GPIO22 to alternative mode 0.
		  //http://www.raspberrypi.org/documentation/hardware/raspberrypi/bcm2835/BCM2835-ARM-Peripherals.pdf
		  //page 102 for an overview of the modes.
		  #If TargetARM And TargetLinux Then
		    soft Declare Function SetMode Lib "libpigpiod_if2.so" Alias "set_mode" (Handle as Integer, gpio as UInteger, mode as UInteger) as Integer
		    Dim result as Integer = SetMode(handle, pin, mode)
		    
		    if result <> 0 then
		      Raise new GPIOException(GPIOException.Operation.SetMode, pin, result)
		    end if
		  #Else
		    #Pragma Unused handle
		    #Pragma Unused pin
		    #Pragma Unused mode
		  #Endif
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetPullUpDown(handle as Integer, pin as UInteger, pud as UInteger)
		  //Sets or clears resistor pull ups or downs on the GPIO.
		  //gpio: 0-53
		  //pud: 0-2
		  //Returns 0 if OK, otherwise PI_BAD_GPIO or PI_BAD_PUD.
		  //gpioSetPullUpDown(17, PUD_UP);   // Sets a pull-up.
		  //gpioSetPullUpDown(18, PUD_DOWN); // Sets a pull-down.
		  //gpioSetPullUpDown(23, PUD_OFF);  // Clear any pull-ups/downs.
		  #If TargetARM And TargetLinux Then
		    soft Declare Function SetPullUpDown Lib "libpigpiod_if2.so" Alias "set_pull_up_down" (pi as Integer, gpio as Uinteger, pud as UInteger) as Integer
		    Dim result as Integer = SetPullUpDown(handle, pin, pud)
		    
		    if result <> 0 then
		      Raise new GPIOException(GPIOException.Operation.SetPullUpDown, pin, result)
		    end if
		  #Else
		    #Pragma Unused handle
		    #Pragma Unused pin
		    #Pragma Unused pud
		  #Endif
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Start(addrStr as Ptr, portStr as Ptr) As Integer
		  //Connect to the pigpio daemon. Reserving command and notification streams.
		  
		  //addrStr: specifies the host or IP address of the Pi running the
		  //pigpio daemon.  It may be NULL in which case localhost
		  //is used unless overridden by the PIGPIO_ADDR environment
		  //variable.
		  
		  //portStr: specifies the port address used by the Pi running the
		  //pigpio daemon.  It may be NULL in which case "8888"
		  //is used unless overridden by the PIGPIO_PORT environment
		  //variable.
		  
		  //Returns an integer value greater than or equal to zero if OK.
		  //This value is passed to the GPIO routines to specify the Pi to be operated on.
		  
		  #If TargetARM And TargetLinux Then
		    soft Declare Function pigpio_start Lib "libpigpiod_if2.so" (addrStr as Ptr, portStr as Ptr) as Integer
		    Return pigpio_start(addrStr, portStr)
		  #Else
		    #Pragma Unused addrStr
		    #Pragma Unused portStr
		  #Endif
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Stop(handle as Integer)
		  //Terminates the connection to a pigpio daemon and releases
		  //resources used by the library.
		  //pi: >=0 (as returned by [*pigpio_start*]).
		  
		  #If TargetARM And TargetLinux Then
		    soft Declare Sub pigpio_stop Lib "libpigpiod_if2.so" (pi as Integer)
		    pigpio_stop(handle)
		  #else
		    #Pragma Unused handle
		  #Endif
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UnregisterCallback(callbackId as UInteger)
		  //This function cancels a callback identified by its id.
		  //callback_id: >=0, as returned by a call to [*callback*] or [*callback_ex*].
		  //The function returns 0 if OK, otherwise pigif_callback_not_found.
		  #If TargetARM And TargetLinux Then
		    Soft Declare Function callback_cancel Lib "libpigpiod_if2.so" (callback_id as UInteger) as Integer
		    Dim result as Integer = callback_cancel(callbackId)
		    
		    if result <> 0 then
		      
		    end if
		  #Else
		    #Pragma Unused callbackId
		  #Endif
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Write(handle as Integer, pin as UInteger, level as UInteger)
		  //Sets the GPIO level, on or off.
		  //pin: 0-53
		  //level: 0-1
		  //Returns 0 if OK, otherwise PI_BAD_GPIO or PI_BAD_LEVEL.
		  //If PWM or servo pulses are active on the GPIO they are switched off.
		  //Arduino style: digitalWrite
		  //gpioWrite(24, 1); // Set GPIO24 high.
		  #If TargetARM And TargetLinux Then
		    soft Declare Function GpioWrite Lib "libpigpiod_if2.so" Alias "gpio_write" (Handle as Integer, gpio as UInteger, mode as UInteger) as Integer
		    Dim result as Integer = GpioWrite(handle, pin, level)
		    
		    if result <> 0 then
		      Raise new GPIOException(GPIOException.Operation.Write, pin, result)
		    end if
		  #Else
		    #Pragma Unused handle
		    #Pragma Unused pin
		    #Pragma Unused level
		  #Endif
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		LibraryVersion As Integer
	#tag EndProperty


	#tag Constant, Name = BAD_CALLBACK, Type = Double, Dynamic = False, Default = \"-2008", Scope = Public
	#tag EndConstant

	#tag Constant, Name = BAD_GPIO, Type = Double, Dynamic = False, Default = \"-3", Scope = Public
	#tag EndConstant

	#tag Constant, Name = BAD_LEVEL, Type = Double, Dynamic = False, Default = \"-5", Scope = Public
	#tag EndConstant

	#tag Constant, Name = BAD_MALLOC, Type = Double, Dynamic = False, Default = \"-2007", Scope = Public
	#tag EndConstant

	#tag Constant, Name = BAD_MODE, Type = Double, Dynamic = False, Default = \"-4", Scope = Public
	#tag EndConstant

	#tag Constant, Name = BAD_PUD, Type = Double, Dynamic = False, Default = \"-6", Scope = Public
	#tag EndConstant

	#tag Constant, Name = CALLBACK_NOTFOUND, Type = Double, Dynamic = False, Default = \"-2010", Scope = Public
	#tag EndConstant

	#tag Constant, Name = DUPLICATE_CALLBACK, Type = Double, Dynamic = False, Default = \"-2006", Scope = Public
	#tag EndConstant

	#tag Constant, Name = EITHER_EDGE, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FALLING_EDGE, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HIGH, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = INPUT, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = LOW, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = OUTPUT, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PI_INIT_FAILED, Type = Double, Dynamic = False, Default = \"-1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PUD_DOWN, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PUD_OFF, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PUD_UP, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RISING_EDGE, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LibraryVersion"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
